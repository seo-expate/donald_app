<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [StudentController::class, 'index']);
Route::get('/student', [StudentController::class, 'index']);
// Route::get('/add_new_student', [StudentController::class, 'create']);
// Route::get('student/{id}/show', [StudentController::class, 'show']);
Route::resource('/student', StudentController::class);


