<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $student = Student::all();
        return view('students.index')->with('students' , $student);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        request()->validate([
            'name' => 'required|max:20|min:3|regex:/^[A-Za-z_-]/',
            //'email' => 'required|email|unique:students',
            'email' => 'required|email',
            'address' => 'required|min:5',
            'phone' => 'required|max:50',
            'num_one' => 'required|max:50',
            'num_two' => 'required|max:50',
            'calculation' => 'required',
            'password' => 'required|min:8|max:100',
        ]);

        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image', $file_name, 'public');
            $input['image'] = $file_name;
        }else{
            $input['image'] = '';
        }

        $input['total'] = '';
        $num_one = floatval($input['num_one']);
        $num_two = floatval($input['num_two']);
        
        if($input['calculation'] == 'plus'){
            $sum_cal = $num_one + $num_two;
            $input['total'] = number_format($sum_cal, 2, '.', '');
        }
        if($input['calculation'] == 'minus'){
            $sum_cal = $num_one - $num_two;
            $input['total'] = number_format($sum_cal, 2, '.', '');
        }
        if($input['calculation'] == 'multiply'){
            $sum_cal = $num_one * $num_two;
            $input['total'] = number_format($sum_cal, 2, '.', '');
        }
        if($input['calculation'] == 'divided'){
            $sum_cal = $num_one / $num_two;
            $input['total'] = number_format($sum_cal, 2, '.', '');
        }

        // unset($input['num_one']);
        // unset($input['num_two']);
        // unset($input['calculation']);

        //echo '<pre>';print_r($input);echo '</pre>';die;

        Student::create($input);
        return redirect('student')->with('flash_message' , "Added");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $student = Student::find($id);
        return view('students.show')->with('student', $student);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        return view('students.edit')->with('students', $student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);
        $input = $request->all();
       
        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image', $file_name, 'public');
            $input['image'] = $file_name;
            Storage::disk('public')->delete('image/' . $input['old_img']);
        }

        request()->validate([
            'name' => 'required|max:20|min:3|regex:/^[A-Za-z_-]/',
            'address' => 'required|min:5',
            'phone' => 'required|max:50',
        ]);

        $num_one = floatval($input['num_one']);
        $num_two = floatval($input['num_two']);

        if($input['calculation'] == 'plus'){
            $sum_cal = $num_one + $num_two;
            $input['total'] = number_format($sum_cal, 2, '.', '');
        }
        if($input['calculation'] == 'minus'){
            $sum_cal = $num_one - $num_two;
            $input['total'] = number_format($sum_cal, 2, '.', '');
        }
        if($input['calculation'] == 'multiply'){
            $sum_cal = $num_one * $num_two;
            $input['total'] = number_format($sum_cal, 2, '.', '');
        }
        if($input['calculation'] == 'divided'){
            $sum_cal = $num_one / $num_two;
            $input['total'] = number_format($sum_cal, 2, '.', '');
        }

        //echo '<pre>';print_r($input);echo '</pre>';die;
        $student->update($input);
        return redirect('student')->with('flash_message', 'Updated');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        Storage::disk('public')->delete('image/' . $student['image']);
        Student::destroy($id);
        return redirect('student')->with('del_message', 'Deleted!');
    }
}
