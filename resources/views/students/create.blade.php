@extends('students.layout')
@section('content')
<div class="col-lg-5 mx-auto mt-3">
    <div class="card">
        <div class="card-header">
            <b>Add New</b>
        </div>
        <div class="card-body">
            <form action="{{ url('student') }}" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control" id="name" aria-describedby="emailHelp">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="text" name="email" value="{{ old('email') }}"class="form-control" id="email" aria-describedby="emailHelp">
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                </div>
                <div class="mb-3">
                    <label for="address" class="form-label">Address</label>
                    <input type="text" name="address" value="{{ old('address') }}" class="form-control" id="address" aria-describedby="emailHelp">
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                </div>
                <div class="mb-3">
                    <label for="phone" class="form-label">Phone No.</label>
                    <input type="text" name="phone" value="{{ old('phone') }}" class="form-control" id="phone">
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                </div>
                <div class="mb-3">
                    <label for="phone" class="form-label">Password</label>
                    <input type="password" name="password" value="{{ old('password') }}" class="form-control" id="phone">
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                </div>
                <div class="mb-3">
                    <label for="image" class="form-label">User Image</label>
                    <input type="file" name="image" value="{{ old('image') }}" class="form-control" id="image">
                    <span class="text-danger">{{ $errors->first('image') }}</span>
                </div>
                <div class="mb-3">
                    <label for="num_one" class="form-label">Number 1</label>
                    <input type="text" name="num_one" value="{{ old('num_one') }}" class="form-control" id="num_one">
                    <span class="text-danger">{{ $errors->first('num_one') }}</span>
                </div>

                <div class="mb-3">
                    <label for="num_one" class="form-label">Calculation</label>
                    <select class="form-select" value="{{ old('calculation') }}" name="calculation" aria-label="Default select example">
                        <option selected value="plus">+</option>
                        <option value="minus">-</option>
                        <option value="multiply">*</option>
                        <option value="divided">/</option>
                    </select>
                </div>

                <div class="mb-3">
                    <label for="num_two" class="form-label">Number 2</label>
                    <input type="text" value="{{ old('num_two') }}" name="num_two" class="form-control" id="num_two">
                    <span class="text-danger">{{ $errors->first('num_two') }}</span>
                </div>
                
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>

</div>
@endsection