@extends('students.layout')
@section('content')
<div class="col-lg-3 mx-auto mt-3">
    <div class="card">
        <div class="card-header">
        <b>Edit</b>
        </div>
        <div class="card-body">
            <form action="{{ url('student/'. $students->id) }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" name="name" class="form-control" value="{{ $students->name }}" id="name" aria-describedby="emailHelp">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>

                <div class="mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="text" name="email" class="form-control" value="{{ $students->email }}" id="email" aria-describedby="emailHelp" disabled>
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                </div>

                <div class="mb-3">
                    <label for="address" class="form-label">Address</label>
                    <input type="text" name="address" class="form-control" value="{{ $students->address }}" id="address" aria-describedby="emailHelp">
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                </div>

                <div class="mb-3">
                    <label for="phone" class="form-label">Phone No.</label>
                    <input type="text" name="phone" class="form-control" value="{{ $students->phone }}" id="phone">
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                </div>

                <input type="hidden" name='old_img' value="{{ $students->image }}">

                <div class="mb-3">
                    <label for="image" class="form-label">User Image</label>
                    <input type="file" name="image" class="form-control" value="{{ $students->image }}" id="image">
                    <span class="text-danger">{{ $errors->first('image') }}</span>
                </div>

                <div class="mb-3">
                    <label for="num_one" class="form-label">Number 1</label>
                    <input type="text" name="num_one" value="{{ $students->num_one }}" class="form-control" id="num_one">
                    <span class="text-danger">{{ $errors->first('num_one') }}</span>
                </div>

                <div class="mb-3">
                    <label for="num_one" class="form-label">Calculation</label>
                    <select class="form-select" value="{{ old('calculation') }}" name="calculation" aria-label="Default select example">
                        <option <?=($students->calculation == 'plus')? 'selected': ''; ?> value="plus">+</option>
                        <option <?=($students->calculation == 'minus')? 'selected': ''; ?> value="minus">-</option>
                        <option <?=($students->calculation == 'multiply')? 'selected': ''; ?> value="multiply">*</option>
                        <option <?=($students->calculation == 'divided')? 'selected': ''; ?> value="divided">/</option>
                    </select>
                </div>

                <div class="mb-3">
                    <label for="num_two" class="form-label">Number 2</label>
                    <input type="text" value="{{ $students->num_two }}" name="num_two" class="form-control" id="num_two">
                    <span class="text-danger">{{ $errors->first('num_two') }}</span>
                </div>
                
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>

</div>
@endsection