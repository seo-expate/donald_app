@extends('students.layout')
@section('content')
<div class="col-lg-5 mx-auto mt-3">
    <div class="card">
        <div class="card-header">
            Details
        </div>
        <div class="card-body">
           <div class="float-start"><img class="border rounded img-fluid" src="{{ asset('/storage/image/'.$student->image) }}"></div>
           <div  class="float-start">
                <div><b>Name: </b> {{ $student->name}}</div>
                <div><b>Email: </b> {{ $student->email}}</div>
                <div><b>Address: </b> {{ $student->address}}</div>
                <div><b>Phone No: </b> {{ $student->phone}}</div>
                <div><b>Total Calculation: </b> {{ $student->num_one}} {{ $student->calculation}} {{ $student->num_two}} = {{ $student->total}}</div>
                <a href=""><button class="btn btn-primary">Change Password</button></a>
           </div>
        </div>
    </div>

</div>
@endsection