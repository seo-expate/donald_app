@extends('students.layout')
@section('content')
<div class="row">
    <div class="col-lg-12 mt-5">
        <a href="/student/create">
            <button type="button" class="btn btn-success">Add New</button>
        </a>

        @if(Session::has('flash_message'))
            <br><br>
            <div class="alert alert-success">
                <h5>{{ Session::get('flash_message') }}</h5>
            </div>
        @endif

        @if(Session::has('del_message'))
            <br><br>
            <div class="alert alert-danger">
                <h5>{{ Session::get('del_message') }}</h5>
            </div>
        @endif

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Total Calculation</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($students as $item)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->num_one}} {{ $item->calculation}} {{ $item->num_two}} = {{ $item->total}}</td>
                    <td>{{ $item->phone }}</td>
                    
                    <td>
                        <a href="{{ url('/student/'. $item->id)}}"><button class="btn btn-sm btn-primary">View</button></a>
                        <a href="{{ url('/student/'. $item->id .'/edit')}}"><button class="btn btn-sm btn-success">Edit</button></a>
                        <form method="POST" action="{{ url('/student' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Student" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
    </div>
</div>
@endsection